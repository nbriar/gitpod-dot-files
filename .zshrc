export ZSH="$HOME/.oh-my-zsh"
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh

ZSH_THEME="gentoo"

plugins=(git)

source $ZSH/oh-my-zsh.sh

. $HOME/.asdf/asdf.sh
